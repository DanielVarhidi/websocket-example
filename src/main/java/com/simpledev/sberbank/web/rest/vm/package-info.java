/**
 * View Models used by Spring MVC REST controllers.
 */
package com.simpledev.sberbank.web.rest.vm;
